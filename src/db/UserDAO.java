package db;


import model.User;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.*;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;

public class UserDAO implements DAO<User> {

	private ArrayList<User> users = new ArrayList<User>();

	@Override
	public ArrayList<User> getAll() {
		try {
			users.clear();
			Connection connection = ConnectionDB.getConnexion();
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT lastName, firstName, mail, company, phoneNumber, active, gender, usertype FROM users");  
			while(rs.next()) {
				this.users.add(new User(rs.getString("lastName"), rs.getString("firstName"), rs.getString("mail"), rs.getString("company"), rs.getString("phoneNumber"), rs.getBoolean("active"), rs.getString("gender"), rs.getString("usertype")));			
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return this.users;
	}

	/* 
	 * return either the User if the password / mail match or null if it doesn't 
	 */
	@Override
	public User get(HashMap<String, String> params) {

		if(params.containsKey("mail")){
			try {
				Connection connection = ConnectionDB.getConnexion();
				String mail = params.get("mail");

				String generatedPassword = null;
				MessageDigest md = MessageDigest.getInstance("SHA-512");
				md.update(params.get("pwd").getBytes());
				byte[] bytes = md.digest();
				StringBuilder sb = new StringBuilder();
				for(int i=0; i< bytes.length ;i++)
				{
					sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
				}
				generatedPassword = sb.toString();
				String sql = "SELECT mail, firstName, lastName, company, phoneNumber, active, gender, usertype FROM users WHERE mail = ? and pwd = ?";

				Statement s = connection.createStatement();
				ResultSet r = s.executeQuery("select pwd from users where mail = '" + mail + "';");

				PreparedStatement st = connection.prepareStatement(sql);
				st.setString(1, mail);
				st.setString(2, generatedPassword);

				ResultSet rs = st.executeQuery();
				if(rs.next()){
					User u = new User(rs.getString("lastName"), rs.getString("firstName"), rs.getString("mail"), rs.getString("company"), rs.getString("phoneNumber"), rs.getBoolean("active"), rs.getString("gender"), rs.getString("usertype"));
					return u;
				}



			} catch (SQLException | NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	@Override
	public void save(User t) {
		Connection connection = ConnectionDB.getConnexion();
		String passwordToHash = t.getPassword();

		try {
			// First hash password
			String generatedPassword = null;
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(passwordToHash.getBytes());
			byte[] bytes = md.digest();
			StringBuilder sb = new StringBuilder();
			for(int i=0; i< bytes.length ;i++)
			{
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = sb.toString();

			// Then save user with salt and hashed password
			String sql = "INSERT INTO users (lastName, firstName, mail, company, phoneNumber, active, gender, usertype, pwd) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
			PreparedStatement preparedStmt = connection.prepareStatement(sql);

			preparedStmt.setString (1, t.getLastName());
			preparedStmt.setString (2, t.getFirstName());
			preparedStmt.setString (3, t.getMail());
			preparedStmt.setString (4, t.getCompany());
			preparedStmt.setString (5, t.getPhoneNumber());
			preparedStmt.setInt (6, 1);
			preparedStmt.setString (7, t.getGender());
			preparedStmt.setString (8, t.getUsertype());
			preparedStmt.setString (9, generatedPassword);

			preparedStmt.execute();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void update(User t, String[] params) {
		//TODO

	}

	@Override
	public void delete(User t) {
		// TODO Auto-generated method stub

	}


	//    @Override
	//    public List<User> getAll(HashMap<String, String> params) {
	//        ArrayList<User> result = new ArrayList<User>();
	//
	//        Connection connection = ConfigConnexion.getConnexion();
	//
	//        String nomTable = this.getNomTable();
	//
	//        String sql = "SELECT mail, nom, prenom, societe, telephone, statut, date_creation, type, cree_par, modifie_par, date_modification " +
	//                    "FROM "+nomTable;
	//        PreparedStatement st = null;
	//        try {
	//            st = connection.prepareStatement(sql);
	//            ResultSet row =  st.executeQuery();
	//            while(row.next()){
	//                Optional<User> u = hydrater(row);
	//                if(u.isPresent()){
	//                    result.add(u.get());
	//                }
	//            }
	//        } catch (SQLException e) {
	//            e.printStackTrace();
	//        }
	//
	//        return  result;
	//
	//    }
	//
	//    @Override
	//    public Optional<User> get(HashMap<String, String> params) {
	//        if(params.containsKey("mail")){
	//
	//            Connection connection = ConfigConnexion.getConnexion();
	//            String nomTable = this.getNomTable();
	//
	//            String mail = params.get("mail");
	//            String sql = "SELECT mail, nom, prenom, societe, telephone, statut, date_creation, type, cree_par, modifie_par, date_modification " +
	//                    "FROM "+nomTable+" " +
	//                    "WHERE mail = ? ";
	//
	//            boolean mdp = false;
	//            if(params.containsKey("mot_de_passe")){
	//                sql += "AND mot_de_passe = ? ";
	//                mdp = true;
	//            }
	//
	//            PreparedStatement st = null;
	//            try {
	//                st = connection.prepareStatement(sql);
	//                st.setString(1, mail);
	//
	//                if(mdp){
	//                    st.setString(2, params.get("mot_de_passe"));
	//                }
	//
	//                ResultSet row =  st.executeQuery();
	//                if(row.next()){
	//                    Optional<User> u = hydrater(row);
	//                    if(u.isPresent()){
	//                        return u;
	//                    }
	//                }
	//            } catch (SQLException e) {
	//                e.printStackTrace();
	//            }
	//
	//        }
	//        return Optional.empty();
	//    }
	//
	//    @Override
	//    public boolean create(User User) {
	//        Connection connection = ConfigConnexion.getConnexion();
	//        String nomTable = this.getNomTable();
	//        String sql = "INSERT INTO "+nomTable+" (mail, mot_de_passe, type, nom, prenom, societe, telephone, statut, date_creation, cree_par) " +
	//                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	//        PreparedStatement st = null;
	//        try {
	//            st = connection.prepareStatement(sql);
	//            st.setString(1, User.getmail());
	//            st.setString(2, User.getMotdepasse());
	//            st.setString(3, User.getType());
	//            st.setString(4, User.getNom());
	//            st.setString(5, User.getPrenom());
	//            st.setString(6, User.getSociete());
	//            st.setString(7, User.getTelephone());
	//            st.setBoolean(8, User.getStatut());
	//
	//            Timestamp timestamp = Timestamp.valueOf(User.getDateCreation());
	//            st.setTimestamp(9, timestamp);
	//
	//            st.setString(10, User.getCreePar().getmail());
	//
	//            int inserted =  st.executeUpdate();
	//            if (inserted > 0){
	//                return true;
	//            }
	//        } catch (SQLException e) {
	//            e.printStackTrace();
	//        }
	//        return false;
	//    }
	//
	//    @Override
	//    public boolean update(User User) {
	//        Connection connection = ConfigConnexion.getConnexion();
	//        String nomTable = this.getNomTable();
	//        String sql = "UPDATE "+nomTable+" " +
	//                    "SET mot_de_passe = ?, nom = ?, prenom = ?, societe = ?, telephone = ?, statut = ?, modifie_par = ?, date_modification = ? " +
	//                    "WHERE mail = ?";
	//        PreparedStatement st = null;
	//        try {
	//            st = connection.prepareStatement(sql);
	//            st.setString(1, User.getMotdepasse());
	//            st.setString(2, User.getNom());
	//            st.setString(3, User.getPrenom());
	//            st.setString(4, User.getSociete());
	//            st.setString(5, User.getTelephone());
	//            st.setBoolean(6, User.getStatut());
	//            st.setString(7, User.getModifiePar().getmail());
	//
	//            Timestamp timestamp = Timestamp.valueOf(User.getDateModification());
	//            st.setTimestamp(8, timestamp);
	//
	//            st.setString(9, User.getmail());
	//
	//            int updated =  st.executeUpdate();
	//            if (updated > 0){
	//                return true;
	//            }
	//        } catch (SQLException e) {
	//            e.printStackTrace();
	//        }
	//        return false;
	//    }
	//
	//    @Override
	//    public boolean delete(User User) {
	//        Connection connection = ConfigConnexion.getConnexion();
	//        String nomTable = this.getNomTable();
	//        String sql = "DELETE FROM "+nomTable+" " +
	//                    "WHERE mail = ?";
	//        PreparedStatement st = null;
	//        try {
	//            st = connection.prepareStatement(sql);
	//            st.setString(1, User.getmail());
	//            int deleted =  st.executeUpdate();
	//            if (deleted > 0){
	//                return true;
	//            }
	//        } catch (SQLException e) {
	//            e.printStackTrace();
	//        }
	//        return false;
	//    }
	//
	//    @Override
	//    public String getNomTable() {
	//        return "User";
	//    }
	//
	//    /**
	//     * Retourne un User hydraté par une ligne récupérée en base
	//     * @param row, la ligne utilisée pour hydrater l'objet User qui va être retourner
	//     * @return
	//     */
	//    private Optional<User> hydrater(ResultSet row){
	//        try {
	//            String mail = row.getString("mail");
	//            String type = row.getString("type");
	//            String nom = row.getString("nom");
	//            String prenom = row.getString("prenom");
	//            String societe = row.getString("societe");
	//            String telephone = row.getString("telephone");
	//            boolean statut = row.getBoolean("statut");
	//            LocalDateTime date_creation = row.getTimestamp("date_creation").toLocalDateTime();
	//            String cree_par = row.getString("cree_par");
	//
	//            Timestamp date_modification_tmstp =  row.getTimestamp("date_modification");
	//            LocalDateTime date_modification =  null;
	//            if(date_modification_tmstp != null){
	//                date_modification = date_modification_tmstp.toLocalDateTime();
	//            }
	//
	//            String modifie_par = row.getString("modifie_par");
	//
	//            User u = null;
	//            if(type.equals("admin")){
	//                u = new Administrateur(mail);
	//            }else{
	//                u = new Stagiaire(mail);
	//            }
	//            u.setNom(nom);
	//            u.setPrenom(prenom);
	//            u.setSociete(societe);
	//            u.setTelephone(telephone);
	//            u.setStatut(statut);
	//            u.setDateCreation(date_creation);
	//            u.setCreePar(new Administrateur(cree_par));
	//            u.setDateCreation(date_modification);
	//            u.setModifiePar(new Administrateur(modifie_par));
	//            return Optional.of(u);
	//
	//        } catch (SQLException e) {
	//            e.printStackTrace();
	//        }
	//        return Optional.empty();
	//    }
}