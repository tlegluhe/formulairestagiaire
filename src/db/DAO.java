package db;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface DAO<T> {
	public List<T> getAll();
	public T get(HashMap<String, String> params);
	void save(T t);
	void update(T t, String[] params);
	void delete(T t);
}