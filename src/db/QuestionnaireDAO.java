package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import model.Answer;
import model.Question;
import model.Questionnaire;
import model.User;

public class QuestionnaireDAO implements DAO<Questionnaire> {

	private ArrayList<Questionnaire> questionnaires = new ArrayList<Questionnaire>();

	@Override
	public ArrayList<Questionnaire> getAll() {
		try {
			questionnaires.clear();
			Connection connection = ConnectionDB.getConnexion();
			Statement stmtQuestionnaire = connection.createStatement();
			ResultSet questionnaireRS = stmtQuestionnaire.executeQuery("SELECT id, subject, active FROM questionnaire");

			while (questionnaireRS.next()) {
				Statement stmtQuestions = connection.createStatement();
				String sql = "SELECT id, heading, active, numorder FROM question WHERE questionnaire = ?";
            	
            	PreparedStatement st = connection.prepareStatement(sql);
                st.setString(1, questionnaireRS.getString("id"));
                ResultSet questionRS = st.executeQuery();
                
                ArrayList<Question> questions = new ArrayList<Question>();
                while(questionRS.next()) {
                	Statement stmtAnswers = connection.createStatement();
    				String answersSQL = "SELECT intitule, correct, active FROM answer WHERE question = ?";
                	
                	PreparedStatement answersStmt = connection.prepareStatement(answersSQL);
                	answersStmt.setString(1, questionRS.getString("id"));
                    ResultSet answerRS = answersStmt.executeQuery();
                    
                    ArrayList<Answer> answers = new ArrayList<Answer>();
                    
                    while(answerRS.next()) {
                    	answers.add(new Answer(answerRS.getString("intitule"), answerRS.getBoolean("correct"), answerRS.getBoolean("active")));
                    }
                	
                	questions.add(new Question(questionRS.getString("heading"), questionRS.getBoolean("active"), questionRS.getInt("numorder"), answers));
                }
				
				this.questionnaires.add(new Questionnaire(questionnaireRS.getInt("id"),questionnaireRS.getString("subject"), questionnaireRS.getBoolean("active"), questions));			
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return this.questionnaires;
	}

	@Override
	public Questionnaire get(HashMap<String, String> params) {
		// TODO Auto-generated method stub
		
		return null;
	}
	
	public Questionnaire getNoneOptionnal(HashMap<String, String> params) {
		if(params.containsKey("id")) {
			try {
				Connection connection = ConnectionDB.getConnexion();
	        	String id = params.get("id");
	        	String sql = "SELECT id, subject, active FROM questionnaire WHERE id = ?";
	        	PreparedStatement st = connection.prepareStatement(sql);
	        	st.setString(1, id);
	        	ResultSet QuestionnaireRS = st.executeQuery();
	        	if(QuestionnaireRS.next()) {
	        		Questionnaire q = new Questionnaire(QuestionnaireRS.getString("subject"));
	        		
	        		sql = "SELECT id, heading, active, numorder FROM question WHERE questionnaire = ?";
	        		st = connection.prepareStatement(sql);
	        		st.setString(1, id);
	        		ArrayList<Question> questionList = new ArrayList<Question>();
	        		ResultSet QuestionRS = st.executeQuery();
	        		if(QuestionRS.next()) {
	        			Question question = new Question(QuestionRS.getString("heading"), 
	        					QuestionRS.getBoolean("active"), QuestionRS.getInt("numorder"));
	        			
	        			questionList.add(question);
	        			sql = "SELECT id, intitule, correct, active FROM answer WHERE question = ?";
	        			PreparedStatement answersStmt = connection.prepareStatement(sql);
	        			answersStmt.setString(1, QuestionRS.getString("id"));
	                    ResultSet answerRS = answersStmt.executeQuery();
	                    ArrayList<Answer> answers = new ArrayList<Answer>();
	                    
		                    while(answerRS.next()) {
		                    	answers.add(new Answer(answerRS.getString("intitule"), answerRS.getBoolean("correct"), answerRS.getBoolean("active")));
		                    }
		                
	                    
	                	questionList.add(new Question(QuestionRS.getString("heading"), QuestionRS.getBoolean("active"), QuestionRS.getInt("numorder"), answers));

	        			
	        		}
	        		
	        		
	        		
	        		
	        		return new Questionnaire(QuestionnaireRS.getInt("id"), QuestionnaireRS.getString("subject"), QuestionnaireRS.getBoolean("active"), questionList);
	        		
	        		
	        	}else
	        		return null;
	        	
	        	
			}catch (Exception e) {
				e.printStackTrace();
			}
        	
        	
			
		}
		return null;
	}

	@Override
	public void save(Questionnaire q) {
		// Saving questionnaire
		try {
			Connection connection = ConnectionDB.getConnexion();
			
			// Inserting request questionnaire
			String sql = "INSERT INTO questionnaire (subject, active) values(?, 1);";
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, q.getSubject());

			// Getting generated id of questionnaire
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating questionnaire failed, no rows affected.");
			}
			long id = -1;
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					id = generatedKeys.getLong(1);
				} else {
					throw new SQLException("Creating questionnaire failed, no ID obtained.");
				}
			}
			
			// Saving questions of questionnaire
			for(Question question : q.getQuestions()) {
				// TODO : numorder
				String insertQuestionSQL = "INSERT INTO question (heading, active, numorder) values(?, 1, 1);";
				PreparedStatement statementQuestion = connection.prepareStatement(insertQuestionSQL, Statement.RETURN_GENERATED_KEYS);
				statement.setString(1, q.getSubject());
			}
			System.out.println("Created questionnaire with id = " + id);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void update(Questionnaire t, String[] params) {
		try {
			Connection connection = ConnectionDB.getConnexion();
			Statement stmt= connection.createStatement();
			String sql = "UPDATE questionnaire "
					+ "SET subject = '" + t.getSubject() + "', "
					+ "active = '" + (t.isActive() ? 1 : 0) + "' "
					+ "WHERE id = ? ";
			PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, params[0]);
            st.execute();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void delete(Questionnaire t) {
		// TODO Auto-generated method stub

	}

}
