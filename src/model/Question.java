package model;

import java.util.ArrayList;

public class Question {
	private String heading;
	private boolean active;
	private int numorder;
	private ArrayList<Answer> answers;
	
	
	public Question(String heading, boolean active, int numorder, ArrayList<Answer> answers) {
		super();
		this.heading = heading;
		this.active = active;
		this.numorder = numorder;
		this.answers = answers;
	}
	
	public Question(String heading, boolean active, int numorder) {
		super();
		this.heading = heading;
		this.active = active;
		this.numorder = numorder;
		// TODO Auto-generated constructor stub
	}

	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public int getNumorder() {
		return numorder;
	}
	public void setNumorder(int numorder) {
		this.numorder = numorder;
	}
	
}
