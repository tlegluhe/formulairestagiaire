package model;

import java.util.ArrayList;

public class Questionnaire {
	private int id;


	private String subject;
	private boolean active;
	private ArrayList<Question> questions;
	
	
	
	public Questionnaire(int id, String subject, boolean active, ArrayList<Question> questions) {
		super();
		this.id = id;
		this.subject = subject;
		this.active = active;
		this.questions = questions;
	}
	
	public Questionnaire(String subject, boolean active, ArrayList<Question> questions) {
		super();
		this.subject = subject;
		this.active = active;
		this.questions = questions;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public Questionnaire(String subject) {
		super();
		this.subject = subject;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public ArrayList<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(ArrayList<Question> questions) {
		this.questions = questions;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
