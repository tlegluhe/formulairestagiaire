package model;

public class User {
	private int id;
	private String lastName;
	private String firstName;
	private String mail;
	private String company;
	private String phoneNumber;
	private Boolean active;
	private String gender;
	private String usertype;
	private String password;
	

	public User(int id, String lastName, String firstName, String mail, String company, String phoneNumber,
			Boolean active, String gender, String usertype, String password) {
		super();
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.mail = mail;
		this.company = company;
		this.phoneNumber = phoneNumber;
		this.active = active;
		this.gender = gender;
		this.usertype = usertype;
		this.password = password;
	}

	public User() { }

	public User(String lastName, String firstName) {
		this.lastName = lastName;
		this.firstName = firstName;
	}

	// full constructor without pwd
	public User(String lastName, String firstName, String mail, String company, String phoneNumber, Boolean active,
			String gender, String usertype) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.mail = mail;
		this.company = company;
		this.phoneNumber = phoneNumber;
		this.active = active;
		this.gender = gender;
		this.usertype = usertype;
	}

	//    full constructor 
	public User(String lastName, String firstName, String mail, String company, String phoneNumber, Boolean active,
			String gender, String usertype, String password) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.mail = mail;
		this.company = company;
		this.phoneNumber = phoneNumber;
		this.active = active;
		this.gender = gender;
		this.password = password;
		this.usertype = usertype;
	}
	
	

	/**
	 * @return the usertype
	 */
	public String getUsertype() {
		return usertype;
	}

	/**
	 * @param usertype the usertype to set
	 */
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [lastName=" + lastName + ", firstName=" + firstName + ", mail=" + mail + ", company=" + company
				+ ", phoneNumber=" + phoneNumber + ", active=" + active + ", gender=" + gender
				+ ", usertype=" + usertype + "]";
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
