package model;

public class Answer {
	private String heading;
	private boolean active;
	private boolean correct;
	
	public Answer(String heading, boolean active, boolean correct) {
		super();
		this.heading = heading;
		this.active = active;
		this.correct = correct;
	}
	
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isCorrect() {
		return correct;
	}
	public void setCorrect(boolean correct) {
		this.correct = correct;
	}
	
}
