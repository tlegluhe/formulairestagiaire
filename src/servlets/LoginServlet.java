package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ConnectionDB;
import db.UserDAO;
import model.User;

@WebServlet(urlPatterns="/Login")
public class LoginServlet extends HttpServlet {
	
	private static UserDAO userDAO = new UserDAO();
	
	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Connection connection = ConnectionDB.getConnexion();
			Statement stmt= connection.createStatement();
			String mail = request.getParameter("mail");
			String password = request.getParameter("password");
			System.out.println("in servlet " + mail +",  " + password);
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("mail", mail);
			params.put("pwd", password);
			
			User maybeUser = userDAO.get(params);
			if(maybeUser != null) {
				User connectedUser = maybeUser;
				response.sendRedirect("accueil.html");
			} else {
				response.sendRedirect("wrongids.html");
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}


	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}
