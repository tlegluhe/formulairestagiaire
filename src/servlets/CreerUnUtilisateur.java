package servlets;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.ConnectionDB;
import db.UserDAO;
import model.User;

@WebServlet(urlPatterns="/CreerUnUtilisateur")
public class CreerUnUtilisateur extends HttpServlet {
	
	private static UserDAO userDAO = new UserDAO();

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 * @throws NoSuchAlgorithmException 
	 */

	private String hashPassword(String password) {
		try {
			SecureRandom random = new SecureRandom();
			byte[] salt = new byte[16];
			random.nextBytes(salt);
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(salt);
			byte[] hashedPasswordBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
			password = new String(hashedPasswordBytes, "UTF-8");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return password;
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		//TODO: something to save the password
		//		System.out.println("password before " + password);
		//		password = hashPassword(password);
		//		System.out.println("password after " + password);

		
		String lastName = request.getParameter("lastName");
		String firstName = request.getParameter("firstName");
		String mail = request.getParameter("mail");
		String company = request.getParameter("company");
		String phoneNumber = request.getParameter("phoneNumber");
		String password = request.getParameter("password");
		
		String gender = request.getParameter("gender");
		
		User toSave = new User(lastName, firstName, mail, company, phoneNumber, true, gender, password);
		this.userDAO.save(toSave);
		
		response.sendRedirect("accueil.html");
//		try (PrintWriter out = response.getWriter()) {
//			/* TODO output your page here. You may use following sample code. */
//			out.println("<!DOCTYPE html>");
//			out.println("<html>");
//			out.println("<head>");
//			out.println("<title>Controller</title>");
//			out.println("</head>");
//			out.println("<body>");
//			out.println("<h1> Utilisateur cr�� !</h1>");
//
//			out.println("</body>");
//			out.println("</html>");
//		}
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//processRequest(request, response);
	}
	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}
}
