package action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.opensymphony.xwork2.ActionSupport;

import db.ConnectionDB;
import db.UserDAO;

public class UserListAction extends ActionSupport{
	
	private ArrayList<model.User> userList;
	private static UserDAO userDAO = new UserDAO();
	
	public String userList() {
		try {
			Connection connection = ConnectionDB.getConnexion();
			setUserList(userDAO.getAll());
			return SUCCESS;
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return ERROR;
	}

	public ArrayList<model.User> getUserList() {
		return userList;
	}

	public void setUserList(ArrayList<model.User> userList) {
		this.userList = userList;
	}

}
