package action;
import java.util.Optional;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import com.opensymphony.xwork2.ActionSupport;

import db.ConnectionDB;
import db.QuestionnaireDAO;
import model.Questionnaire;

public class questionnaireListAction extends ActionSupport{
	
	private ArrayList<model.Questionnaire> questionnaireList;
	private static QuestionnaireDAO questionnaireDAO = new QuestionnaireDAO();
	private String questionnaireID;
	
	public String questionnaireList() {
		try {
			setQuestionnaireList(questionnaireDAO.getAll());
			return SUCCESS;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return ERROR;
	}
	
	public String disableOrEnableQuestionnaire() {
		try {
			Connection connection = ConnectionDB.getConnexion();
			Statement stmt= connection.createStatement();
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("id",questionnaireID);
			Questionnaire q = questionnaireDAO.getNoneOptionnal(params);
			if(q != null) {
				
				q.setActive(!q.isActive());
				questionnaireDAO.update(q, new String[]{questionnaireID});
				return SUCCESS;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return ERROR;
	}

	/**
	 * @return the questionnaireList
	 */
	public ArrayList<model.Questionnaire> getQuestionnaireList() {
		return questionnaireList;
	}

	/**
	 * @param questionnaireList the questionnaireList to set
	 */
	public void setQuestionnaireList(ArrayList<model.Questionnaire> questionnaireList) {
		this.questionnaireList = questionnaireList;
	}

	public String getQuestionnaireID() {
		return questionnaireID;
	}

	public void setQuestionnaireID(String questionnaireID) {
		this.questionnaireID = questionnaireID;
	}
	
	
	


}
