package action;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

import db.QuestionnaireDAO;
import model.Question;
import model.Questionnaire;

public class CreerUnQuestionnaire extends ActionSupport {
	private static QuestionnaireDAO questionnaireDAO = new QuestionnaireDAO();
	private String subject;
	
	public String createQuestionnaire() {		
		Questionnaire toSave = new Questionnaire(subject, true, new ArrayList<Question>());

		questionnaireDAO.save(toSave);
		return SUCCESS;
	}

	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}

	
}
