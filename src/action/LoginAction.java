package action;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.config.entities.ActionConfig;

import db.ConnectionDB;
import model.User;
import db.UserDAO;

public class LoginAction extends ActionSupport implements SessionAware{
	private String mail;
	private String password;
	private model.User connectedUser;
	private static UserDAO userDAO = new UserDAO();
	private Map<String, Object> session;
	
	public String login() {
		session = ActionContext.getContext().getSession();
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("mail", mail);
		params.put("pwd", password);
		
		User maybeUser = userDAO.get(params);

		if(maybeUser != null) {
			connectedUser = maybeUser;
			this.session.put("connectedUser", connectedUser);
			if(connectedUser.getUsertype().equals("admin"))
				return "success_admin";
			else
				return "success_intern";
			
		} else {
			return "error";
		}
	}

	public model.User getConnectedUser() {
		return connectedUser;
	}

	public void setConnectedUser(model.User connectedUser) {
		this.connectedUser = connectedUser;
	}
	
	
	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		this.session = session;
	}
	

}
