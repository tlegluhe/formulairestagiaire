package action;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import com.opensymphony.xwork2.ActionSupport;

import db.UserDAO;
import model.User;

public class CreerUnUtilisateur extends ActionSupport {
	private static UserDAO userDAO = new UserDAO();
	private String firstName;
	private String lastName;
	private String mail;
	private String company;
	private String phoneNumber;
	private String password;
	private String gender;
	private String usertype;
		

	/**
	 * The action method
	 * @return name of view
	 */
	public String createUser() {
		
		User toSave = new User(lastName, firstName, mail, company, phoneNumber, true, gender, usertype, password);

		userDAO.save(toSave);
		return SUCCESS;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getCompany() {
		return company;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getUsertype() {
		return usertype;
	}


	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
}


