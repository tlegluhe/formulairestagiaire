<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<title>Formulaire</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="style/main.css" />

</head>
<body>
<div class="container">
	<h1>Active quizzes</h1>
	<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Quiz</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <s:iterator value="questionnaireList" var="questionnaire">
        
	        <s:if test="%{#questionnaire.active==1}">
	        	<tr>
	        		<td><s:property value="%{#questionnaire.subject}"/></td>
	        		<td>
	        			<s:url var="test" action="enableOrDisableQuestionnaire">
						  <s:param name="questionnaireID" value="%{#questionnaire.id}"></s:param>
						</s:url>
						<a href="${test}" class="btn btn-danger">Disable</a>
	        		</td>
	        	</tr>
	       	</s:if>
        </s:iterator>
            
        </tbody>
        <tfoot>
            <tr>
                <th>Quiz</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
    
    <h1>Inactive quizzes</h1>
    
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Quiz</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <s:iterator value="questionnaireList" var="questionnaire">
	        <s:if test="%{#questionnaire.active==0}">
	        	<tr>
	        		<td><s:property value="%{#questionnaire.subject}"/></td>
	        		<td>
	        			<s:url var="test" action="enableOrDisableQuestionnaire">
						  <s:param name="questionnaireID" value="%{#questionnaire.id}"></s:param>
						</s:url>
						<a href="${test}" class="btn btn-success">Enable</a>
	        		</td>
	        	</tr>
	       	</s:if>
        </s:iterator>
            
        </tbody>
        <tfoot>
            <tr>
                <th>Quiz</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
</div>
</body>
</html>


<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable();
	});
</script>
