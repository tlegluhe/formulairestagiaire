<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" charset="text/html; utf-8" />
<title>Formulaires</title>
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
</head>

<body class="vertical-center">
	<div class="col-md-4 offset-md-4">
		<s:form class="form-signin" action="loginAction">
			<h1 class="h3 mb-3 font-weight-normal">Please sign in !</h1>
			<s:textfield type="mail" id="mail" class="form-control"
				placeholder="Adresse email" name="mail" />

			<s:textfield type="password" id="password" class="form-control"
				placeholder="Mot de passe" name="password" />
				<s:submit value="Submit" class="btn btn-success btn-block mb-2 mt-2" />
		</s:form>
			<div class="form-signin">
				<a class="btn btn-secondary btn-block" href="formulaire.html">Créer
					un compte</a>
			</div>
	</div>
</body>
</html>
