<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<title>Formulaire</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="style/main.css" />

</head>
<body>
<div class="container">
	<h1>Liste des utilisateurs</h1>
	<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Mail</th>
                <th>Company</th>
                <th>Phone number</th>
                <th>Gender</th>
                <th>User type</th>
                <th>Account status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <s:iterator value="userList" var="user">
        	<tr>
        		<td><s:property value="%{#user.lastName}"/></td>
        		<td><s:property value="%{#user.firstName}"/></td>
        		<td><s:property value="%{#user.mail}"/></td>
        		<td><s:property value="%{#user.company}"/></td>
        		<td><s:property value="%{#user.phoneNumber}"/></td>
        		<td><s:property value="%{#user.gender}"/></td>
        		<td><s:property value="%{#user.usertype}"/></td>
        		<s:if test="%{#user.active==1}">
					<td>Enabled</td>
					<td><s:a class="btn btn-danger" action="">Disable</s:a></td>
				</s:if>
				<s:else>
					<td>Disabled</td>
					<td><s:a class="btn btn-success" action="">Enable</s:a></td>
				</s:else>
				
        		
        	</tr>
        </s:iterator>
            
        </tbody>
        <tfoot>
            <tr>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Mail</th>
                <th>Company</th>
                <th>Phone number</th>
                <th>Gender</th>
                <th>User type</th>
                <th>Account status</th>
                <th>Action</th>
                
            </tr>
        </tfoot>
    </table>
</div>
</body>
</html>


<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable();
	});
</script>
