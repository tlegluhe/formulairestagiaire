<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Créer questionnaire</title>
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
</head>

<body class="container">
	<s:form class="form-signin" action="CreerUnQuestionnaire" method="post">
		<h1 class="h3 mb-3 font-weight-normal">Créer un questionnaire</h1>

		<label for="subject">Sujet</label>
		<s:textfield type="text" id="subject" class="form-control"
			placeholder="" name="subject" />

		<s:submit class="btn btn-lg btn-primary btn-block" value="Submit" />
	</s:form>
</body>
</html>