<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<title>Formulaire</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="style/main.css" />

</head>
<body>
	<div>Accueil</div>
	<div>Vous êtes connectés</div>
	<s:property value="connectedUser.usertype"/>
	<div class="">
	<a href="login.html">Voir liste questionnaires</a>
	<a href="formulaire.html">Visualisation anciens résultats</a>
	<a href="accueil.html">Retour accueil</a>
	<a href="index.html">Se déconnecter</a>
	</div>
</body>
</html>
