<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
<title>Formulaires Stagiaires</title>
</head>
<body class="container">
	<s:form class="form-signin" action="CreerUnUtilisateur" method="post">
		<h1 class="h3 mb-3 font-weight-normal">Create account</h1>

		<s:textfield type="text" id="firstName" class="form-control"
			placeholder="Prénom" name="firstName" />

		<s:textfield type="text" class="form-control"
			placeholder="Nom de famille" name="lastName" />

		<s:textfield type="text" class="form-control" placeholder="Email"
			name="mail" />

		<s:textfield type="text" class="form-control" placeholder="Entreprise"
			name="company" />

		<s:textfield type="text" class="form-control" placeholder="Téléphone"
			name="phoneNumber" />

		<s:textfield type="password" class="form-control"
			placeholder="Mot de passe" name="password" />
			
		<s:select label="Choose user type" name="usertype" list="#{'intern':'Intern', 'admin':'Admin'}"/>
		
		<s:select label="Choose gender" name="gender" list="#{'male':'Male', 'female':'Female'}"/>


		<s:submit class="btn btn-lg btn-primary btn-block"/>
	</s:form>

</body>

</html>
