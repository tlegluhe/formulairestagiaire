<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<title>Formulaire</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="style/main.css" />

</head>
<body>
<div class="container">
	<h1>Bienvenue sur la page administrateur</h1>
	<div>Vous êtes connectés</div>
	<s:property value="connectedUser.mail"/>
	<div class="">
	
	<s:a class="btn btn-primary" action="userListAction">Consulter les utilisateurs</s:a>
	<s:a class="btn btn-primary" href="newUserFormulaire.jsp">Ajouter un nouvel utilisateur</s:a>
	<s:a class="btn btn-primary" href="newQuestionnaireFormulaire.jsp">Créer un questionnaire</s:a>
	</div>
	<div class="">
		<s:a class="btn btn-primary" action="questionnaireListAction">Consulter les utilisateurs</s:a>
	</div>
</div>
	
</body>
</html>
